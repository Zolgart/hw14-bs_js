/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
    При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/ 



/*
1.В чому полягає відмінність localStorage і sessionStorage?
 localStorage зберігає дані назавжди в браузері, а sessionStorage - лише протягом одного сеансу.

2.Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
Ніколи не зберігайте чутливу інформацію у localStorage або sessionStorage без шифрування.

3.Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
Дані в sessionStorage втрачаються після завершення сеансу браузера.
*/

const buttonElement = document.querySelector(".change-theme");
const btnCollection = document.querySelectorAll(".btn");

const storedTheme = sessionStorage.getItem("theme");
console.log(storedTheme)

if (storedTheme) {
    document.body.classList.add(storedTheme);
    btnCollection.forEach(btn => btn.classList.add("storedTheme"));
}

buttonElement.addEventListener("click", () => {

    const hasBlueTheme = document.body.classList.contains("blue-theme");

    if (hasBlueTheme) {
        document.body.classList.remove("blue-theme");
        btnCollection.forEach(btn => btn.classList.remove("blue-theme"));
        sessionStorage.removeItem("theme");
    } else {
        document.body.classList.add("blue-theme");
        btnCollection.forEach(btn => btn.classList.add("blue-theme"));
        sessionStorage.setItem("theme", "blue-theme");
    }
});

window.addEventListener('beforeunload', () => {
    if (hasBlueTheme) {
        sessionStorage.setItem("theme", "blue-theme");
    }
});
